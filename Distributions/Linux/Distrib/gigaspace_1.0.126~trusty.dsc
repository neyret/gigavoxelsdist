Format: 1.0
Source: gigaspace
Binary: libgigaspace, gigaspace-dev, gigaspace-demo
Architecture: any
Version: 1.0.126~trusty
Maintainer: Pascal Guehl <pascal.guehl@inria.fr>
Homepage: http://artis.imag.fr/Membres/~Pascal.Guehl/
Standards-Version: 3.9.3
Build-Depends: debhelper (>= 9), qt4-dev-tools, libqt4-dev, libqt4-opengl-dev, libqglviewer-dev, freeglut3-dev, libmagick++-dev, libtinyxml-dev, libqwt-dev, cimg-dev, libassimp-dev, libglm-dev, unzip, cmake, nvidia-cuda-toolkit (>= 5), libdcmtk2-dev
Package-List: 
 gigaspace-demo deb devel standard
 gigaspace-dev deb devel standard
 libgigaspace deb devel standard
Checksums-Sha1: 
 821f273b2ec5c6a4f52102d1dd36b7dec86ebcbe 810530926 gigaspace_1.0.126~trusty.tar.gz
Checksums-Sha256: 
 94a40d7616235f5acc1b5a9b1ba39d43e16d1cd93925da73221f6dbc851fc217 810530926 gigaspace_1.0.126~trusty.tar.gz
Files: 
 3d13c9da72fc24c8c63ab42034c6fdf1 810530926 gigaspace_1.0.126~trusty.tar.gz
